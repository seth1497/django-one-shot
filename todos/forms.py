from django.forms import ModelForm
from .models import TodoList, TodoItem


class TodoListForm(ModelForm):
    class Meta:
        model = TodoList
        fields = ('name',)
        # fields = ("title", "picture", "description")

class TodoItemForm(ModelForm):
    class Meta:
        model = TodoItem
        fields = ('task', 'due_date', 'is_completed', 'list')
        # widgets = {
        #    # Here you define what input type should the field have
        #    'color': Select(attrs = {'class': 'form-control',})
        #    }
