from django.urls import path
from todos.views import todo_view, todo_show_details, create_todo_list, update_todo_list, delete_todo_list, create_todo_item, update_todo_item


urlpatterns = [
    path("", todo_view, name="todo_list_list"),
    path("create/", create_todo_list, name="todo_list_create"),
    path("<int:id>/", todo_show_details, name="todo_list_detail"),
    path("<int:id>/edit/", update_todo_list, name="todo_list_update"),
    path("<int:id>/delete/", delete_todo_list, name="todo_list_delete"),
    path("items/create/", create_todo_item, name="todo_item_create"),
    path("items/<int:id>/edit/", update_todo_item, name="todo_item_update")
]
