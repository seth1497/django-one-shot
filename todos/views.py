from django.shortcuts import get_object_or_404, render, redirect
from .models import TodoList, TodoItem
from .forms import TodoListForm, TodoItemForm

# Create your views here.

def todo_view(request):
    todo_list = TodoList.objects.all()
    context = {"todo_list": todo_list}
    return render(request, "lists.html", context)

def todo_show_details(request, id):
    todo_object = TodoList.objects.get(id=id)
    context = {
        "todo_object": todo_object
    }
    return render(request, "detail.html", context)


def delete_todo_list(request, id):
    list = TodoList.objects.get(id=id)
    if request.method == "POST":
        list.delete()
        return redirect("todo_list_list")
    return render(request, "delete.html")

def create_todo_list(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            list = form.save()
            return redirect("todo_list_detail", id=list.id)
            # id=item.list.id
    else:
        form = TodoListForm()
    context = {"form": form}
    return render(request, "create.html", context)

def create_todo_item(request):
    # item = TodoItem.objects.get(id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", id=item.list.id)
    else:
        form = TodoItemForm()
    context = {
        "form": form,
        # "item": item
    }
    return render(request, "create_item.html", context)

def update_todo_list(request, id):
    list = TodoList.objects.get(id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=list)
        if form.is_valid():
            list = form.save()
            return redirect("todo_list_detail", id=list.id)
            # id=item.list.id
    else:
        form = TodoListForm(instance=list)
    context = {"form": form}
    # context = {"form": form, "todolist": list}
    return render(request, "edit.html", context)


def update_todo_item(request, id):
    item = TodoItem.objects.get(id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=item)
        if form.is_valid():
      # To redirect to the detail view of the model, use this:
            item = form.save()
            return redirect("todo_list_detail", id=item.list.id)

      # To add something to the model, like setting a user,
      # use something like this:
      #
      # model_instance = form.save(commit=False)
      # model_instance.user = request.user
      # model_instance.save()
      # return redirect("detail_url", id=model_instance.id)
    else:
        form = TodoItemForm(instance=item)

    context = {
        "form": form
    }

    return render(request, "edit_item.html", context)



# def create_model_name(request):
#   if request.method == "POST":
#     form = ModelForm(request.POST)
#     if form.is_valid():
#       # To redirect to the detail view of the model, use this:
#       model_instance = form.save()
#       return redirect("detail_url", id=model_instance.id)

#       # To add something to the model, like setting a user,
#       # use something like this:
#       #
#       # model_instance = form.save(commit=False)
#       # model_instance.user = request.user
#       # model_instance.save()
#       # return redirect("detail_url", id=model_instance.id)
#   else:
#     form = ModelForm()

#   context = {
#     "form": form
#   }

#   return render(request, "model_names/create.html", context)

# def delete_model_name(request, id):
#   model_instance = ModelName.objects.get(id=id)
#   if request.method == "POST":
#     model_instance.delete()
#     return redirect("some_url")

#   return render(request, "models_names/delete.html")

# def edit_recipe(request, id):
#     post = get_object_or_404(Recipe, id=id)
#     # post = Recipe.objects.get(id=id)
#     if request.method == "POST":
#         form = RecipeForm(request.POST, instance=post)
#         if form.is_valid():
#             form.save()
#             return redirect("recipes_list")
#     else:
#         form = RecipeForm(instance=post)
#     context = {"form": form, "recipe": post}
#     return render(request, "recipes/edit.html", context)




# def create_recipe(request):
#     if request.method == "POST":
#         form = RecipeForm(request.POST)
#         if form.is_valid():
#             form.save()
#             return redirect("recipes_list")
#     else:
#         form = RecipeForm()
#     context = {"form": form}
#     return render(request, "recipes/create.html", context)


# def show_recipe(request, id):
#     a_really_cool_recipe = get_object_or_404(Recipe, id=id)
#     context = {
#         "recipe_object": a_really_cool_recipe
#     }
#     # print(context)
#     return render(request, 'recipes/detail.html', context)
